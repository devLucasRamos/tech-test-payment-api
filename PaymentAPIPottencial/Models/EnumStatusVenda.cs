﻿namespace PaymentAPIPottencial.Models
{
    public enum EnumStatusVenda
    {
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada
    }
}
