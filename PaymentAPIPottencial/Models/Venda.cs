﻿namespace PaymentAPIPottencial.Models
{
    public class Venda
    {
        public int Id_Venda { get; set; }
        public DateTime DT_Venda { get; set; }
        public EnumStatusVenda Status { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<PedidoItem> Itens { get; set; }

    }
}
