﻿namespace PaymentAPIPottencial.Models
{
    public class PedidoItem
    {
        public int Id { get; set; }
        public int Quantidade { get; set;}
        public decimal ValorUnitario { get; set; }
        public Produto Produto { get; set; }
    }
}
